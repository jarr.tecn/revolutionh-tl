![REvolutionH-tl logo.](https://gitlab.com/jarr.tecn/revolutionh-tl/-/raw/master/docs/images/Logo_horizontal.png)

# Example

Here we present an example of how to run REvolutionH-tl.

## The data

We will reconstruct the evolution of the proteins in 4 bacteria. We downloaded their proteomes from NCBI:

- [Citricoccus zhacaiensis (NCBI Taxonomy ID 489142)](https://www.ncbi.nlm.nih.gov/datasets/taxonomy/489142/)
- [Corynebacterium felinum (NCBI Taxonomy ID 131318)](https://www.ncbi.nlm.nih.gov/datasets/taxonomy/131318/)
- [Sutcliffiella horikoshii (NCBI Taxonomy ID 79883)](https://www.ncbi.nlm.nih.gov/datasets/taxonomy/79883/)
- [Bacillus thuringiensis (NCBI Taxonomy ID 527031)](https://www.ncbi.nlm.nih.gov/datasets/taxonomy/527031/)

Additionally, we obtained the species tree for these organisms using the tool [*Common tree* of NCBI](https://www.ncbi.nlm.nih.gov/Taxonomy/CommonTree/wwwcmt.cgi).

We stored the fasta files and the species tree in Google Drive, [click here to download](https://drive.google.com/file/d/1PzbJMXsMaY0uZ2EQtVqixXZVZto5s6_M/view?usp=sharing).

## Run analysis

We will work in the same directory where the data is stored

```bash
cd example_data
```

Here you will find:

- a directory `fasta_files` containing the fasta files,
- a file `species_tree.phy` containing a species tree,
- `diamond` is the diamond executable program, and
- a directory `example_output_files` containing an example of output files.

Now, run REvolutionH-tl as follows:

```bash
python -m revolutionhtl -F fasta_files -S species_tree.phy -aligner ./diamond
```

In this command ,we specify the fastas directory using `-F fasta_files`, a species tree `-S species_tree.phy`, and we specify the alignment program using `-aligner ./diamond`.

In the case you don't have a species tree, you can omit the attribute `-S species_tree.phy`, in such a case, REvolutionH-tl will use the species tree generated in step 4. Note that the names in the species tree is identical to the name of the fasta files.

The command above runs in 87.03 seconds. It produces the following output files:

- tl_project.best_hits.tsv
- tl_project_log.txt
- tl_project.best_matches.tsv
- tl_project.orthogroups.tsv
- tl_project.corrected_trees.tsv
- tl_project.orthologs.tsv
- tl_project.gene_trees.tsv
- tl_project.reconciliation.tsv
- tl_project.labeled_species_tree.nhxx
- tl_project.species_tree.tsv
- tl_project_alignment_all_vs_all

Go to the [main documentation](https://pypi.org/project/revolutionhtl/) for a detailed description of these files, and their interpretation.

As shown in the box below, the command will also display in the terminal messages concerning the progress of the program.

```
  _ \  __|            |        |   _)              |  |      |    |
    /  _| \ \ /  _ \  |  |  |   _|  |   _ \    \   __ | ____| _|  |
 _|_\ ___| \_/ \___/ _| \_,_| \__| _| \___/ _| _| _| _|     \__| _|
V1.1.2

Running steps 1, 2, 3, 4, 5

Step 1: Alignment hits computation
----------------------------------
Generating index: 100%|██████████████████████████████████| 4/4 [00:00<00:00,  8.48it/s]
Running alignments: 100%|████████████████████████████████| 6/6 [00:20<00:00,  3.48s/it]
Alignment hits successfully written to tl_project_alignment_all_vs_all/
This data will be used as input of step 2

Step 2: Best hit selection
--------------------------
Input hits: tl_project_alignment_all_vs_all/
Reading .proteinortho.tsv file and hits directory...
Selecting best hits by dynamic threshold...
100%|█████████████████████████████████████████| 30084/30084 [00:02<00:00, 13384.20it/s]
Best hits successfully written to tl_project.best_hits.tsv
This file will be used as input for step 3
Orthogroups successfully written to tl_project.orthogroups.tsv

Step 3: Gene tree reconstruction and orthology inference
--------------------------------------------------------
Processing best hit graphs of step 2...
100%|█████████████████████████████████████████████| 2341/2341 [00:18<00:00, 129.92it/s]
Reconstructing gene trees...


| Reconstructing evolution for families with 1 <= number of genes <= 100
| This represents the 99.91 % of the data
| At the end of this bunch, 99.91 % of the data will be analyzed.

Reconstructing gene trees...
100%|█████████████████████████████████████████████| 2339/2339 [00:06<00:00, 376.34it/s]
Labeling gene tree with evolutionary events...
100%|████████████████████████████████████████████| 2339/2339 [00:00<00:00, 6555.17it/s]
Best match graphs successfully written to tl_project.best_matches.tsv
Gene trees successfully written to tl_project.gene_trees.tsv


| Reconstructing evolution for families with 101 <= number of genes <= 200
| This represents the 0.04 % of the data
| At the end of this bunch, 99.96 % of the data will be analyzed.

Reconstructing gene trees...
100%|████████████████████████████████████████████████████| 1/1 [00:00<00:00,  4.35it/s]
Labeling gene tree with evolutionary events...
100%|███████████████████████████████████████████████████| 1/1 [00:00<00:00, 117.43it/s]
Best match graphs successfully written to tl_project.best_matches.tsv
Gene trees successfully written to tl_project.gene_trees.tsv


| Reconstructing evolution for families with 201 <= number of genes <= 300
| This represents the 0.04 % of the data
| At the end of this bunch, 100.00 % of the data will be analyzed.

Reconstructing gene trees...
100%|████████████████████████████████████████████████████| 1/1 [00:01<00:00,  1.41s/it]
Labeling gene tree with evolutionary events...
100%|████████████████████████████████████████████████████| 1/1 [00:00<00:00, 40.74it/s]
Best match graphs successfully written to tl_project.best_matches.tsv
Gene trees successfully written to tl_project.gene_trees.tsv
These gene trees will be used as input for step 4
These gene trees will be used as input for step 5
100%|████████████████████████████████████████████| 2341/2341 [00:00<00:00, 9025.43it/s]
Orthologs successfully written to tl_project.orthologs.tsv

Step 4: Species tree reconstruction
-----------------------------------
Using gene trees generated at step 3...
Reconstructing species tree...
Species tree successfully written to tl_project.species_tree.tsv
This species tree will be used as input for step 5

Step 5: Gene and species trees reconciliation
---------------------------------------------
Using gene trees generated at step 3...
Using species tree generated at step 4...
Editing inconsistent gene trees...
100%|█████████████████████████████████████████████| 2341/2341 [00:05<00:00, 447.87it/s]
Corrected gene trees successfully written to tl_project.corrected_trees.tsv
Reconciling trees...
100%|████████████████████████████████████████████| 2341/2341 [00:01<00:00, 1637.30it/s]
Reconciliation successfully written to tl_project.reconciliation.tsv
Indexed species tree successfully written to tl_project.labeled_species_tree.nhxx

REvolutionH-tl finished all the tasks without any problem
---------------------------------------------------------

```



## Running a single step

```bash
python -m revolutionhtl -steps 2 --alignment_hits tl_project_alignment_all_vs_all -singletons -F fasta_files
```

```
  _ \  __|            |        |   _)              |  |      |    |
    /  _| \ \ /  _ \  |  |  |   _|  |   _ \    \   __ | ____| _|  |
 _|_\ ___| \_/ \___/ _| \_,_| \__| _| \___/ _| _| _| _|     \__| _|
V1.1.2

Running steps 2

Step 2: Best hit selection
--------------------------
Input hits: tl_project_alignment_all_vs_all/
Input fasta files: fasta_files/
Reading .proteinortho.tsv file and hits directory...
Selecting best hits by dynamic threshold...
100%|█████████████████████████████████████████| 30084/30084 [00:01<00:00, 15101.42it/s]
Best hits successfully written to tl_project.best_hits.tsv
Orthogroups successfully written to tl_project.orthogroups.tsv
singletons successfully written to tl_project.singletons.tsv

REvolutionH-tl finished all the tasks without any problem
---------------------------------------------------------
```

## Using BLAST

Use "blastp" if your fasta files contain protein sequences, or "blastn" when you have gene sequences.

```bash
python -m revolutionhtl -F fasta_files -aligner blastp
```

```
  _ \  __|            |        |   _)              |  |      |    |
    /  _| \ \ /  _ \  |  |  |   _|  |   _ \    \   __ | ____| _|  |
 _|_\ ___| \_/ \___/ _| \_,_| \__| _| \___/ _| _| _| _|     \__| _|
V1.1.2

Running steps 1, 2, 3, 4, 5

Step 1: Alignment hits computation
----------------------------------
Generating index:   0%|                                          | 0/4 [00:00<?, ?it/s]

Building a new DB, current time: 11/11/2023 10:48:57
New DB name:   /home/jarr/Documents/code/revolutionh-tl/docs/example_data/tl_project_alignment_all_vs_all/Citricoccus_zhacaiensis
New DB title:  fasta_files/Citricoccus_zhacaiensis.fa
Sequence type: Protein
Keep MBits: T
Maximum file size: 1000000000B
Adding sequences from FASTA; added 3252 sequences in 0.320618 seconds.


Generating index:  25%|████████▌                         | 1/4 [00:00<00:01,  1.99it/s]

Building a new DB, current time: 11/11/2023 10:48:58
New DB name:   /home/jarr/Documents/code/revolutionh-tl/docs/example_data/tl_project_alignment_all_vs_all/Sutcliffiella_horikoshii
New DB title:  fasta_files/Sutcliffiella_horikoshii.fa
Sequence type: Protein
Keep MBits: T
Maximum file size: 1000000000B
Adding sequences from FASTA; added 4190 sequences in 0.275492 seconds.


Generating index:  50%|█████████████████                 | 2/4 [00:00<00:00,  2.14it/s]

Building a new DB, current time: 11/11/2023 10:48:58
New DB name:   /home/jarr/Documents/code/revolutionh-tl/docs/example_data/tl_project_alignment_all_vs_all/Corynebacterium_felinum
New DB title:  fasta_files/Corynebacterium_felinum.fa
Sequence type: Protein
Keep MBits: T
Maximum file size: 1000000000B
Adding sequences from FASTA; added 2565 sequences in 0.151827 seconds.


Generating index:  75%|█████████████████████████▌        | 3/4 [00:01<00:00,  2.50it/s]

Building a new DB, current time: 11/11/2023 10:48:59
New DB name:   /home/jarr/Documents/code/revolutionh-tl/docs/example_data/tl_project_alignment_all_vs_all/Bacillus_thuringiensis
New DB title:  fasta_files/Bacillus_thuringiensis.fa
Sequence type: Protein
Keep MBits: T
Maximum file size: 1000000000B
Adding sequences from FASTA; added 6175 sequences in 0.368758 seconds.


Generating index: 100%|██████████████████████████████████| 4/4 [00:01<00:00,  2.22it/s]
Running alignments: 100%|███████████████████████████████| 6/6 [13:29<00:00, 134.92s/it]
Alignment hits successfully written to tl_project_alignment_all_vs_all/
This data will be used as input of step 2

Step 2: Best hit selection
--------------------------
Input hits: tl_project_alignment_all_vs_all/
Reading .proteinortho.tsv file and hits directory...
Selecting best hits by dynamic threshold...
100%|███████████████████████████████████████| 147534/147534 [00:09<00:00, 15512.79it/s]
Best hits successfully written to tl_project.best_hits.tsv
This file will be used as input for step 3
Orthogroups successfully written to tl_project.orthogroups.tsv

Step 3: Gene tree reconstruction and orthology inference
--------------------------------------------------------
Processing best hit graphs of step 2...
100%|█████████████████████████████████████████████| 2213/2213 [00:14<00:00, 156.80it/s]
Reconstructing gene trees...


| Reconstructing evolution for families with 1 <= number of genes <= 100
| This represents the 99.64 % of the data
| At the end of this bunch, 99.64 % of the data will be analyzed.

Reconstructing gene trees...
100%|█████████████████████████████████████████████| 2205/2205 [00:06<00:00, 334.89it/s]
Labeling gene tree with evolutionary events...
100%|████████████████████████████████████████████| 2205/2205 [00:00<00:00, 5502.12it/s]
Best match graphs successfully written to tl_project.best_matches.tsv
Gene trees successfully written to tl_project.gene_trees.tsv


| Reconstructing evolution for families with 101 <= number of genes <= 200
| This represents the 0.27 % of the data
| At the end of this bunch, 99.91 % of the data will be analyzed.

Reconstructing gene trees...
100%|████████████████████████████████████████████████████| 6/6 [00:01<00:00,  4.06it/s]
Labeling gene tree with evolutionary events...
100%|███████████████████████████████████████████████████| 6/6 [00:00<00:00, 123.37it/s]
Best match graphs successfully written to tl_project.best_matches.tsv
Gene trees successfully written to tl_project.gene_trees.tsv


| Reconstructing evolution for families with 201 <= number of genes <= 300
| This represents the 0.05 % of the data
| At the end of this bunch, 99.95 % of the data will be analyzed.

Reconstructing gene trees...
100%|████████████████████████████████████████████████████| 1/1 [00:00<00:00,  1.31it/s]
Labeling gene tree with evolutionary events...
100%|████████████████████████████████████████████████████| 1/1 [00:00<00:00, 60.78it/s]
Best match graphs successfully written to tl_project.best_matches.tsv
Gene trees successfully written to tl_project.gene_trees.tsv


| Reconstructing evolution for families with 301 <= number of genes <= 400
| This represents the 0.05 % of the data
| At the end of this bunch, 100.00 % of the data will be analyzed.

Reconstructing gene trees...
100%|████████████████████████████████████████████████████| 1/1 [00:03<00:00,  3.08s/it]
Labeling gene tree with evolutionary events...
100%|████████████████████████████████████████████████████| 1/1 [00:00<00:00, 28.79it/s]
Best match graphs successfully written to tl_project.best_matches.tsv
Gene trees successfully written to tl_project.gene_trees.tsv
These gene trees will be used as input for step 4
These gene trees will be used as input for step 5
100%|████████████████████████████████████████████| 2213/2213 [00:00<00:00, 6203.73it/s]
Orthologs successfully written to tl_project.orthologs.tsv

Step 4: Species tree reconstruction
-----------------------------------
Using gene trees generated at step 3...
Reconstructing species tree...
Species tree successfully written to tl_project.species_tree.tsv
This species tree will be used as input for step 5

Step 5: Gene and species trees reconciliation
---------------------------------------------
Using gene trees generated at step 3...
Using species tree generated at step 4...
Editing inconsistent gene trees...
100%|█████████████████████████████████████████████| 2213/2213 [00:03<00:00, 573.81it/s]
Corrected gene trees successfully written to tl_project.corrected_trees.tsv
Reconciling trees...
100%|████████████████████████████████████████████| 2213/2213 [00:01<00:00, 1462.09it/s]
Reconciliation successfully written to tl_project.reconciliation.tsv
Indexed species tree successfully written to tl_project.labeled_species_tree.nhxx

REvolutionH-tl finished all the tasks without any problem
---------------------------------------------------------
```
