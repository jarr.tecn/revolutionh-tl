![REvolutionH-tl logo.](https://gitlab.com/jarr.tecn/revolutionh-tl/-/raw/master/docs/images/Logo_horizontal.png)

# `revolutionhtl.orthology` usage

```bash
python -m revolutionhtl.orthology <trees> <arguments>
```

# Arguments

```bash
positional arguments:
  trees                 .tsv file containing a gene tree in .nhx format (new newick) for each row in the column specified by -c

options:
  -h, --help            show this help message and exit
  -tc TREE_COLUMN, --tree_column TREE_COLUMN
                        column name for gene trees in the input .tsv file. (default="tree")
  -oc ORTHOGROUP_COLUMN, --orthogroup_column ORTHOGROUP_COLUMN
                        column name for gene trees ID in the input .tsv file. (default="OG")
  -l LOSS_LEAFS, --loss_leafs LOSS_LEAFS
                        Label of the leafs associated with a gene loss event. (default="X")
  -o OUTPUT_PREFIX, --output_prefix OUTPUT_PREFIX
                        Prefix used for output files (default "tl_project").
```