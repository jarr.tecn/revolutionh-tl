
_df_matches_cols= ['Query_accession',
                  'Target_accession',
                        'Sequence_identity',
                        'Length',
                        'Mismatches',
                        'Gap_openings',
                        'Query_start',
                        'Query_end',
                        'Target_start',
                        'Target_end',
                        'E_value',
                        'Bit_score',
                       ]

_diamond_tl_headers= ['Query_accession',
                      'Target_accession',
                      'Query_length',
                      'Target_length',
                      'Alignment_length',
                      'Bit_score',
                      'Evalue',
                     ]

_default_f= 0.95
