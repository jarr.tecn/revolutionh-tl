from .TreePolytomies import TreePolytomies
from .DMSeries import compute_distance_matrix
from .NanNeighborJoining import resolve_tree_with_nan
from .parse_prt import load_all_hits_raw, normalize_scores
from .nhx_tools import get_nhx, read_nhx

import re
import pandas as pd
import numpy as np
import networkx as nx
from Bio import Phylo
from io import StringIO


def refine_df(distances, gTrees, label_attr= 'label'):
    # Identify trees containing polytomies
    polytomies= gTrees.tree.apply(TreePolytomies, label_attr= label_attr)
    mask= polytomies.apply(lambda X: len(X.polytomi_partition) > 0)
    unresolved= pd.DataFrame(dict(tree= gTrees.tree[mask].to_list(),
                                  polytomies= polytomies[mask].to_list()))

    # Resolve polytomies
    F = lambda row: process_tree_polytomies(row.tree, row.polytomies, distances)
    resolved = pd.DataFrame(unresolved.apply(F, axis=1).to_list(),
                                  columns=['tree', 'polytomies', 'resolved'])

    # Write to nhx
    G = lambda T: get_nhx(T, T.u_lca, name_attr='accession')
    resolved.tree = resolved.tree.apply(G)

    return resolved

def process_tree_polytomies(tree, polytomies, distance_pairs):

    X: list[int] = polytomies.get_nodes_with_polytomies()

    resolutions= []

    for x in X:
        Y: list[int] = polytomies.get_ys(x)
        C: list[list[str]] = [polytomies.get_cluster(x, y_i) for y_i in Y]
        # Compute the distance matrix for the NJ algorithm
        D, _, info = compute_distance_matrix(distance_pairs, C, Y)

        if is_diagonal_zero_and_nan_elsewhere(D):
            resolved_subtree_newick= None #!!!
        else:
            resolved_subtree_newick = resolve_tree_with_nan(
                D, [str(y) if isinstance(y, int) else y for y in Y], x)

            tree = update_tree_with_newick(
                tree, node=x, newick_str=resolved_subtree_newick)

        resolutions+= [resolved_subtree_newick]

    return tree, len(resolutions), len([x for x in resolutions if x!=None])


def tree_to_string(D: nx.DiGraph, node, level=0, prefix="", show_labels=True) -> str:
    # Get the label of the node if it exists, otherwise use the node ID
    label = D.nodes[node].get('label', str(node))

    # Create the current line of the tree
    connector = "|-" if level > 0 else ""  # Add '|-' only if not the root
    line = f"{prefix}{connector}{node} ({label})" if show_labels else f"{prefix}{connector}{node}"

    # Initialize the tree string with the current node
    tree_str = line + "\n"

    # Prepare the prefix for the next level
    new_prefix = prefix + ("| " if level > 0 else "  ")  # Add '| ' or '  '

    # Recurse for children and accumulate the result
    children = list(D.successors(node))
    for i, child in enumerate(children):
        is_last = i == len(children) - 1  # Adjust prefix for the last child
        child_prefix = new_prefix if not is_last else prefix + "  "
        tree_str += tree_to_string(D, child, level + 1, child_prefix, show_labels)

    return tree_str

def update_tree_with_newick(D, node, newick_str) -> nx.DiGraph:
    """
    Resolve a polytomy in a NetworkX DiGraph using a Newick string and return a new graph.

    Args:
        D (nx.DiGraph): Original directed graph.
        node (int): Node with a polytomy to resolve.
        newick_str (str): Newick string representing the resolved subtree.

    Returns:
        nx.DiGraph: A new graph with the resolved polytomy.
    """
    # Create a copy of the original graph
    new_graph = D.copy()

    # Parse the Newick string into a tree
    handle = StringIO(newick_str)
    tree = Phylo.read(handle, "newick")

    # Generate new nodes for internal nodes introduced in the Newick tree
    new_node_id = max(new_graph.nodes) + 1  # Start creating new nodes from max existing ID + 1
    new_node_map = {}

    def map_newick_clade(clade):
        nonlocal new_node_id
        if clade.is_terminal():
            # Terminal nodes are the same as the original nodes
            return int(clade.name)
        else:
            # Create a new internal node
            new_internal_node = new_node_id
            new_node_id += 1
            new_node_map[clade] = new_internal_node
            return new_internal_node

    # Traverse the Newick tree to add the resolved structure
    def add_newick_edges(clade, parent):
        if clade.is_terminal():
            # Terminal node: add edge to the terminal node
            return int(clade.name)
        else:
            # Internal node: add edge to a new internal node
            internal_node = map_newick_clade(clade)
            for child in clade.clades:
                child_node = add_newick_edges(child, internal_node)
                new_graph.add_edge(internal_node, child_node)
            return internal_node

    # Remove old edges from the polytomy node in the new graph
    for child in list(new_graph.successors(node)):
        new_graph.remove_edge(node, child)

    # Add the resolved structure to the graph
    root_clade = tree.clade
    clades = root_clade.clades

    if len(clades) >= 1:
        # First child of the Newick tree becomes a direct child of `node`
        first_child = add_newick_edges(clades[0], node)
        new_graph.add_edge(node, first_child)

    if len(clades) > 1:
        # Second clade introduces a new internal node
        new_internal_node = new_node_id
        new_node_id += 1
        new_graph.add_edge(node, new_internal_node)
        # Add the 'label' attribute to the new_internal_node.
        new_graph.nodes[new_internal_node]['label'] = 'D'       # Discussed with Toño. 17/12/2024
        new_graph.nodes[new_internal_node]['node_id'] = None    # TODO: Could be new_internal_node - 1
        new_graph.nodes[new_internal_node]['species'] = None

        # Add the subtrees rooted at the second clade
        for child_clade in clades[1].clades:
            child_node = add_newick_edges(child_clade, new_internal_node)
            new_graph.add_edge(new_internal_node, child_node)

    return new_graph

def transform_newick(input_newick):
    """
    Transforms a Newick string by reordering attributes inside square brackets.

    Specifically, the function:
      - Reorders attributes in the format: 'label[node_id=...;species=...]'
      - Handles attributes appearing in any order: species, node_id, label
      - Ensures the output format always starts with 'label' followed by 'node_id' and 'species'.

    Args:
        input_newick (str): The original Newick string.

    Returns:
        str: The transformed Newick string with reordered attributes.
    """

    # Function to parse and reorder attributes
    def reorder_attributes(match):
        """
        Extracts and reorders attributes found within square brackets.

        Args:
            match (re.Match): A regex match object containing attributes inside square brackets.

        Returns:
            str: A string with reordered attributes in the desired format.
        """
        content = match.group(1)  # Get the full content inside the brackets
        # Split the attributes by ';' and create a key-value dictionary
        attributes = dict(kv.split('=') for kv in content.split(';') if kv)

        # Extract specific attributes (order is important)
        species = attributes.get('species', None)
        node_id = attributes.get('node_id', None)
        label = attributes.get('label', None)

        # Construct the replacement string in the desired order
        replacement = f"{label or ''}[node_id={node_id or ''}"
        if species:
            replacement += f";species={species}"
        replacement += "]"

        return replacement

    # Regex pattern to match anything inside square brackets '[...]'
    pattern = re.compile(r'\[(.*?)\]')

    # Apply the transformation using the nested reorder_attributes function
    output_newick = pattern.sub(reorder_attributes, input_newick)

    return output_newick

def is_diagonal_zero_and_nan_elsewhere(D: np.ndarray) -> bool:
    """
    Checks if a matrix has zeros on the diagonal and NaN everywhere else.

    Args:
        D (np.ndarray): The input matrix.

    Returns:
        bool: True if the matrix satisfies the condition, False otherwise.
    """

    # Check if diagonal elements are all zeros
    diagonal_zero = np.all(np.diag(D) == 0)

    # Create a mask for the off-diagonal elements
    off_diagonal_mask = ~np.eye(D.shape[0], dtype=bool)  # True for off-diagonal elements

    # Check if off-diagonal elements are all NaN
    off_diagonal_nan = np.all(np.isnan(D[off_diagonal_mask]))

    return diagonal_zero and off_diagonal_nan
