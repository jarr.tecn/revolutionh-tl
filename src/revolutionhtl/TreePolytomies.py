import networkx as nx
from revolutionhtl.nhx_tools import get_nhx
from revolutionhtl.nxTree import induced_colors

def is_polytomi(T, node):
    return len(T[node]) > 2

def get_polytomies(T):
    return [node for node in T if is_polytomi(T, node)]

class TreePolytomies:
    def __init__(self, tree: nx.DiGraph, label_attr= 'label'):
        self.polytomi_partition= {x : {y_i : list(induced_colors(tree, y_i, label_attr))
                                       for y_i in tree.successors(x)}
                                  for x in get_polytomies(tree)}

    def get_nodes_with_polytomies(self) -> list[int]:
        return list(self.polytomi_partition.keys())

    def get_ys(self, x: int) -> list[int]:
        return list(self.polytomi_partition[x].keys())

    def get_cluster(self, x, y_i) -> list[int | str]:
        return self.polytomi_partition[x][y_i]
